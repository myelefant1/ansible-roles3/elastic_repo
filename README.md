## Elastic Repository

This role adds elasticsearch repository.

Tested on debian 10.

## Role parameters

| name                        | value    | optionnal | default value   | description                              |
| ----------------------------|----------|-----------|-----------------|-----------------------------------------|
| elastic_major_version       | string   | yes       | 7               | Elasticsearch major version added to apt repository|

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/elastic_repo.git
  scm: git
  version: master
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: elastic_repo
```

## Tests

[tests/tests_elastic_repo](tests/tests_elastic_repo)
